pipeline {
  agent {
    docker {
      image 'cimg/android:2024.11.1'
      args '-v gradle_wrapper:/tmp/gradle_wrapper:rw -v gradle_caches:/tmp/gradle_caches:rw'
    }
  }
  parameters {
    string(name: 'RELEASE', defaultValue: '', description: 'The release branch to build and publish from')
    booleanParam(name: 'BB_DOWNLOADS', defaultValue: false, description: 'Whether to upload to Downloads section of BitBucket repo')
  }
  stages {
    stage('Prepare') {
      steps {
        sh 'mkdir "$GRADLE_USER_HOME"'
        sh 'ln -s /tmp/gradle_caches "${GRADLE_USER_HOME}/caches"'
        sh 'ln -s /tmp/gradle_wrapper "${GRADLE_USER_HOME}/wrapper"'
      }
    }
    stage('Build') {
      steps {
        sh './gradlew --no-daemon clean assembleRelease'
      }
    }
    stage('Sign') {
      steps {
        sh 'zipalign -v -p 4 "$APK_UNSIGNED" "$APK_ALIGNED"'
        withCredentials([file(credentialsId: 'simsik-khipoo-keystore', variable: 'KEYSTORE'), string(credentialsId: 'simsik-khipoo-keystore-password', variable: 'KEYSTORE_PASS'), usernamePassword(credentialsId: 'simsik-khipoo-release-key', passwordVariable: 'KEY_PASS', usernameVariable: 'KEY_ALIAS')]) {
          sh 'apksigner sign --ks "$KEYSTORE" --ks-pass "pass:$KEYSTORE_PASS" --ks-key-alias "$KEY_ALIAS" --key-pass "pass:$KEY_PASS" --out "$APK_SIGNED" "$APK_ALIGNED"'
        }
        archiveArtifacts artifacts: 'app/build/outputs/apk/release/app-release.apk', fingerprint: true
      }
    }
    stage('Upload') {
      steps {
        androidApkUpload googleCredentialsId: 'simsik-khipoo-google-play', apkFilesPattern: 'app/build/outputs/apk/release/app-release.apk', trackName: 'internal'
      }
    }
    stage('BitBucket') {
      when {
        expression { return params.BB_DOWNLOADS }
      }
      steps {
        sh 'cp "$APK_SIGNED" "${APK_PATH}/app-release-v${params.RELEASE}.apk"'
        withCredentials([usernameColonPassword(credentialsId: 'simsik-khipoo-bitbucket', variable: 'BB_AUTH')]) {
          sh 'curl -X POST --user "${BB_AUTH}" "https://api.bitbucket.org/2.0/repositories/iongchun/simsik-khipoo/downloads" --form files=@"${APK_PATH}/app-release-v${params.RELEASE}.apk"'
        }
      }
    }
  }
  environment {
    GRADLE_USER_HOME = '/tmp/gradle'
    APK_PATH = 'app/build/outputs/apk/release'
    APK_UNSIGNED = 'app/build/outputs/apk/release/app-release-unsigned.apk'
    APK_ALIGNED = 'app/build/outputs/apk/release/app-release-unsigned-aligned.apk'
    APK_SIGNED = 'app/build/outputs/apk/release/app-release.apk'
  }
}
