/*
Copyright 2017 Âng Iōngchun

This file is part of Sim-sik ê Khí-pooh.

Sim-sik ê Khí-pooh is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sim-sik ê Khí-pooh is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sim-sik ê Khí-pooh.  If not, see <http://www.gnu.org/licenses/>.
 */
package tw.iongchun.taigikbd;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by iongchun on 9/21/17.
 */

public class TKDatabase extends SQLiteOpenHelper {
	public static final String DATABASE_NAME = "taigikbd.db";
	public static final int DATABASE_VERSION = 5;

	public TKDatabase(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	//public class

	@Override
	public void onConfigure(SQLiteDatabase db) {
		super.onConfigure(db);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		createVersion5(db);
	}

	private void createVersion5(SQLiteDatabase db) {
		createVersion4(db);
		upgradeVersion4To5(db);
	}

	private void upgradeVersion4To5(SQLiteDatabase db) {
		db.execSQL("create table metadata (name text, value text)");
		upgradeVersion4To5(db, "words");
		upgradeVersion4To5(db, "words_poj");
	}

	private void upgradeVersion4To5(SQLiteDatabase db, String wordTable) {
		db.execSQL("alter table " + wordTable + " add fusion_id int");
		db.execSQL("create index idx_" + wordTable + "_fusion_id on " + wordTable + " (fusion_id)");
		db.execSQL("alter table " + wordTable + " add name_id int");
		db.execSQL("create index idx_" + wordTable + "_name_id on " + wordTable + " (name_id)");
		db.execSQL("alter table " + wordTable + " add surname_id int");
		db.execSQL("create index idx_" + wordTable + "_surname_id on " + wordTable + " (surname_id)");
	}

	private void createVersion4(SQLiteDatabase db) {
		createVersion4(db, "words");
		createVersion4(db, "words_poj");
	}

	private void createVersion4(SQLiteDatabase db, String wordTable) {
		db.execSQL("create table " + wordTable + " (word text, han text, toneless text, phrase_id int, alt_id int, weight real, primary key(word, han))");
		db.execSQL("create index idx_" + wordTable + "_with_weight on " + wordTable + " (word, weight)");
		db.execSQL("create index idx_" + wordTable + "_toneless_weight on " + wordTable + " (toneless, weight)");
		db.execSQL("create index idx_" + wordTable + "_phrase_id on " + wordTable + " (phrase_id)");
		db.execSQL("create index idx_" + wordTable + "_alt_id on " + wordTable + " (alt_id)");
		db.execSQL("create index idx_" + wordTable + "_word on " + wordTable + " (word)");
		db.execSQL("create index idx_" + wordTable + "_han on " + wordTable + " (han)");
	}

	private void upgradeVersion3To4(SQLiteDatabase db) {
		// words
		db.execSQL("alter table words add alt_id default NULL");
		db.execSQL("create index idx_words_alt_id on words (alt_id)");
		// words_poj
		db.execSQL("alter table words_poj add alt_id default NULL");
		db.execSQL("create index idx_words_poj_alt_id on words_poj (alt_id)");
	}

	private void createVersion3(SQLiteDatabase db) {
		// words
		db.execSQL("create table words (word text, han text, toneless text, phrase_id int, weight real, primary key(word, han))");
		db.execSQL("create index idx_words_with_weight on words (word, weight)");
		db.execSQL("create index idx_words_toneless_weight on words (toneless, weight)");
		db.execSQL("create index idx_words_phrase_id on words (phrase_id)");
		db.execSQL("create index idx_words_word on words (word)");
		db.execSQL("create index idx_words_han on words (han)");
		// words_poj
		db.execSQL("create table words_poj (word text, han text, toneless text, phrase_id int, weight real, primary key(word, han))");
		db.execSQL("create index idx_words_poj_with_weight on words_poj (word, weight)");
		db.execSQL("create index idx_words_poj_toneless_weight on words_poj (toneless, weight)");
		db.execSQL("create index idx_words_poj_phrase_id on words_poj (phrase_id)");
		db.execSQL("create index idx_words_poj_word on words_poj (word)");
		db.execSQL("create index idx_words_poj_han on words_poj (han)");
	}

	private void createVersion2(SQLiteDatabase db) {
		// words
		db.execSQL("create table words (word text primary key, toneless text, phrase_id int, han text, weight real)");
		db.execSQL("create index idx_words_with_weight on words (word, weight)");
		db.execSQL("create index idx_words_toneless_weight on words (toneless, weight)");
		db.execSQL("create index idx_words_phrase_id on words (phrase_id)");
		db.execSQL("create index idx_words_han on words (han)");
		// words_poj
		db.execSQL("create table words_poj (word text primary key, toneless text, phrase_id int, han text, weight real)");
		db.execSQL("create index idx_words_poj_with_weight on words_poj (word, weight)");
		db.execSQL("create index idx_words_poj_toneless_weight on words_poj (toneless, weight)");
		db.execSQL("create index idx_words_poj_phrase_id on words_poj (phrase_id)");
		db.execSQL("create index idx_words_poj_han on words_poj (han)");
	}

	private void createVersion1(SQLiteDatabase db) {
		// words
		db.execSQL("create table words (word text primary key, toneless text, phrase_id int, weight real)");
		db.execSQL("create index idx_words_with_weight on words (word, weight)");
		db.execSQL("create index idx_words_toneless_weight on words (toneless, weight)");
		db.execSQL("create index idx_words_phrase_id on words (phrase_id)");
		// words_poj
		db.execSQL("create table words_poj (word text primary key, toneless text, phrase_id int, weight real)");
		db.execSQL("create index idx_words_poj_with_weight on words_poj (word, weight)");
		db.execSQL("create index idx_words_poj_toneless_weight on words_poj (toneless, weight)");
		db.execSQL("create index idx_words_poj_phrase_id on words_poj (phrase_id)");
	}

	static boolean checkTableExist(SQLiteDatabase db, String name) {
		Cursor c = db.rawQuery("SELECT count(*) FROM sqlite_master WHERE type='table' AND name=?", new String[] {name});
		c.moveToNext();
		int count = c.getInt(0);
		boolean exists = count > 0;
		c.close();
		return exists;
	}

	private void backupTable(SQLiteDatabase db, String name) {
		if (checkTableExist(db, name)) {
			String nameOrg = name + "_org";
			db.execSQL("drop table if exists " + nameOrg);
			db.execSQL("alter table " + name + " rename to " + nameOrg);
		}
		db.execSQL("drop index if exists idx_" + name + "_with_weight");
		db.execSQL("drop index if exists idx_" + name + "_toneless_weight");
		db.execSQL("drop index if exists idx_" + name + "_phrase_id");
		db.execSQL("drop index if exists idx_" + name + "_word");
		db.execSQL("drop index if exists idx_" + name + "_han");
		db.execSQL("drop index if exists idx_" + name + "_alt_id");
		db.execSQL("drop index if exists idx_" + name + "_fusion_id");
		db.execSQL("drop index if exists idx_" + name + "_name_id");
		db.execSQL("drop index if exists idx_" + name + "_surname_id");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// special migration
		if (newVersion == 5 && oldVersion >= 3) {
			if (oldVersion == 3)
				upgradeVersion3To4(db);
			upgradeVersion4To5(db);
			return;
		}

		// normal migration
		if (newVersion > oldVersion) {
			backupTable(db, "words");
			backupTable(db, "words_poj");
		}
		switch (newVersion) {
		case 1:
			createVersion1(db);
			break;
		case 2:
			createVersion2(db);
			break;
		case 3:
			createVersion3(db);
			break;
		case 4:
			createVersion4(db);
			break;
		case 5:
			createVersion5(db);
			break;
		}
	}

	@Override
	public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// special migration
		if (newVersion < oldVersion) {
			backupTable(db, "words");
			backupTable(db, "words_poj");
			if (newVersion == 4) // downgrade from 5+
				db.execSQL("drop table if exists metadata");
		}
		switch (newVersion) {
			case 1:
				createVersion1(db);
				break;
			case 2:
				createVersion2(db);
				break;
			case 3:
				createVersion3(db);
				break;
			case 4:
				createVersion4(db);
				break;
			case 5:
				createVersion5(db);
				break;
		}
	}

	@Override
	public void onOpen(SQLiteDatabase db) {
		super.onOpen(db);
	}
}
