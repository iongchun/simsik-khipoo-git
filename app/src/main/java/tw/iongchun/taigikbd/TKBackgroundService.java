/*
Copyright 2017 Âng Iōngchun

This file is part of Sim-sik ê Khí-pooh.

Sim-sik ê Khí-pooh is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sim-sik ê Khí-pooh is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sim-sik ê Khí-pooh.  If not, see <http://www.gnu.org/licenses/>.
 */
package tw.iongchun.taigikbd;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import org.supercsv.io.CsvListReader;
import org.supercsv.prefs.CsvPreference;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;
import java.util.concurrent.atomic.AtomicBoolean;

import static androidx.core.app.NotificationCompat.PRIORITY_DEFAULT;

/**
 * Created by iongchun on 9/21/17.
 */

public class TKBackgroundService extends IntentService {
	public static final String ACTION = "action";
	public static final int ACTION_UPDATE_DATABASE = 1;
	public static final int ACTION_INCREMENT_WORD_WEIGHT = 2;
	public static final String WORD = "word";
	public static final String INTENT_DB_READY = "tw.iongchun.taigikbd.DATABASE_READY";
	private static final int DATA_VERSION = 1;
	private static final int PHRASE_ID_MAX = 29601;
	private static final int ALT_ID_MAX = 1663;
	private static final int FUSION_ID_MAX = 37;
	private static final int NAME_ID_MAX = 16866;
	private static final int SURNAME_ID_MAX = 2531;
	private static final int WORDS_TOTAL = 82818;
	private boolean appDebug;
	private SQLiteDatabase db;

	public TKBackgroundService() {
		super("bg");
	}

	private static final AtomicBoolean dbStop = new AtomicBoolean(false);

	public static void updateDatabase(Context context) {
		Intent intent = new Intent(context, TKBackgroundService.class);
		intent.putExtra(ACTION, ACTION_UPDATE_DATABASE);
		dbStop.set(false);
		context.startService(intent);
	}

	public static void terminateDatabase() {
		dbStop.set(true);
	}

	public static void incrementWordWeight(Context context, String word) {
		Intent intent = new Intent(context, TKBackgroundService.class);
		intent.putExtra(ACTION, ACTION_INCREMENT_WORD_WEIGHT);
		intent.putExtra(WORD, word);
		context.startService(intent);
	}

	private static String wordTailoNormalized(String word) {
		StringBuilder buf = new StringBuilder();
		StringTokenizer toks = new StringTokenizer(word, "-", true);
		while (toks.hasMoreTokens()) {
			String sylb = toks.nextToken();
			if (sylb.equals("-")) {
				buf.append('-');
				continue;
			}
			TKInputState tailo = TKComposingUtils.parseState(sylb);
			String sylbTl = TKComposingUtils.getComposingTextTL(tailo);
			buf.append(sylbTl);
		}
		return buf.toString();
	}

	private static String wordTailoToPoj(String word) {
		StringBuilder buf = new StringBuilder();
		StringTokenizer toks = new StringTokenizer(word, "-", true);
		while (toks.hasMoreTokens()) {
			String sylb = toks.nextToken();
			if (sylb.equals("-")) {
				buf.append('-');
				continue;
			}
			TKInputState tailo = TKComposingUtils.parseState(sylb);
			TKInputState poj = TKComposingUtils.tailoToPoj(tailo);
			String sylbPoj = TKComposingUtils.getComposingTextPOJ(poj);
			buf.append(sylbPoj);
		}
		return buf.toString();
	}

	@Override
	public void onCreate() {
		if (getPackageName().endsWith("debug"))
			appDebug = true;
		db = new TKDatabase(this).getWritableDatabase();
		super.onCreate();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		db.close();
	}

	protected void log(String text) {
		if (appDebug) {
			String pfx = String.valueOf(System.currentTimeMillis() % (60 * 60 * 1000)) + ' ';
			pfx += "[" + Thread.currentThread().getName() + "] ";
			Intent intent = new TKLogIntent(this, pfx + text);
			startService(intent);
		}
	}

	protected void log(String text, Object... args) {
		if (args != null && args.length >= 1) {
			StringBuilder sb = new StringBuilder(text);
			for (Object arg : args) {
				if (arg == null)
					sb.append("(null)");
				else
					sb.append(arg.toString());
			}
			text = sb.toString();
		}
		log(text);
	}

	protected void broadcastDatabaseReady() {
		Intent intent = new Intent(INTENT_DB_READY);
		LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
	}

	@Override
	protected void onHandleIntent(Intent workIntent) {
		if (workIntent == null)
			return;
		switch (workIntent.getIntExtra(ACTION, 0)) {
			case ACTION_UPDATE_DATABASE:
				updateDatabase();
				break;
			case ACTION_INCREMENT_WORD_WEIGHT:
				String word = workIntent.getStringExtra(WORD);
				incrementWordWeight(word);
				break;
		}
	}

	private String queryMetaData(String name, String defaultValue) {
		try (Cursor c = db.rawQuery("select value from metadata where name=?", new String[]{name})) {
			if (c.moveToNext()) {
				return c.getString(0);
			}
			return defaultValue;
		}
	}

	private int queryMetaDataAsInt(String name, int defaultValue) {
		String value = queryMetaData(name, null);
		if (value == null)
			return defaultValue;
		try {
			return Integer.parseInt(value);
		} catch (NumberFormatException e) {
			return defaultValue;
		}
	}

	public static final String METADATA_DATA_VERSION = "data_version";

	private int queryDataVersion() {
		return queryMetaDataAsInt(METADATA_DATA_VERSION, Integer.MIN_VALUE);
	}

	private void setMetaData(String name, String value) {
		ContentValues values = new ContentValues();
		values.put("name", name);
		values.put("value", value);
		db.insertWithOnConflict("metadata",null, values, SQLiteDatabase.CONFLICT_REPLACE);
	}

	private void setDataVersion(int value) {
		setMetaData(METADATA_DATA_VERSION, String.valueOf(value));
	}

	private void setDataVersion() {
		setDataVersion(DATA_VERSION);
	}

	private int queryLastId(String idName) {
		Cursor c = db.rawQuery("select max(" + idName + ") from words", new String[] {});
		c.moveToNext();
		int id = 0;
		if (!c.isNull(0))
			id = c.getInt(0);
		c.close();
		return id;
	}

	private int queryWordCount(String table) {
		Cursor c = db.rawQuery("select count(*) from " + table, new String[] {});
		c.moveToNext();
		int count = c.getInt(0);
		c.close();
		return count;
	}

	private int queryWordCount() {
		return queryWordCount("words") + queryWordCount("words_poj");
	}

	private void updateDatabase() {
		int curPhraseId = 0;
		int curAltId = 0;
		int curFusionId = 0;
		int curNameId = 0;
		int curSurnameId = 0;
		int dataVersion = queryDataVersion();
		// TODO: test
		//int dataVersion = 0;
		log("data version: ", dataVersion);
		if (dataVersion == DATA_VERSION) {
			int curWords = queryWordCount();
			log("word count: ", curWords);
			curPhraseId = queryLastId("phrase_id");
			curAltId = queryLastId("alt_id");
			curFusionId = queryLastId("fusion_id");
			curNameId = queryLastId("name_id");
			curSurnameId = queryLastId("surname_id");
			if (curPhraseId >= PHRASE_ID_MAX
					&& curAltId >= ALT_ID_MAX
					&& curFusionId >= FUSION_ID_MAX
					&& curNameId >= NAME_ID_MAX
					&& curSurnameId >= SURNAME_ID_MAX) {
				broadcastDatabaseReady();
				return;
			}
		} else {
			// data version is changed, clear all
			db.delete("words", null, null);
			db.delete("words_poj", null, null);
			setDataVersion();
		}

		NotificationManager notifManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
		NotificationCompat.Builder notifBuilder = createNotification();
		notifBuilder.setProgress(WORDS_TOTAL, 0, false);
		notifManager.notify(TKNotification.NOTIF_DICT_DB, notifBuilder.build());

		boolean result = true;
		if (curPhraseId < PHRASE_ID_MAX)
			result = updateMain(curPhraseId, notifBuilder);
		if (result && curAltId < ALT_ID_MAX)
			result = updateAlts(curAltId, "04_又唸作.csv", "alt_id", notifBuilder);
		if (result && curFusionId < FUSION_ID_MAX)
			result = updateAlts(curFusionId, "05_合音唸作.csv", "fusion_id", notifBuilder);
		if (result && curNameId < NAME_ID_MAX)
			result = updateWords("09_名.csv", "name_id", curNameId, notifBuilder);
		if (result && curSurnameId < SURNAME_ID_MAX)
			result = updateWords("10_姓.csv", "surname_id", curSurnameId, notifBuilder);

		if (result) {
			if (dbStop.get()) {
				notifBuilder.setContentText(getResources().getString(R.string.dict_db_cancel));
			} else {
				migrateTable("words");
				migrateTable("words_poj");
				broadcastDatabaseReady();
				notifBuilder.setContentText(getResources().getString(R.string.dict_db_done));
			}
		} else {
			notifBuilder.setContentText(getResources().getString(R.string.dict_db_error));
		}
		notifBuilder.setProgress(0, 0, false);
		notifManager.notify(TKNotification.NOTIF_DICT_DB, notifBuilder.build());
	}

	private NotificationCompat.Builder createNotification() {
		NotificationCompat.Builder notifBuilder = new NotificationCompat.Builder(this, TKNotification.CHANNEL_INFO);
		notifBuilder.setSmallIcon(android.R.drawable.ic_dialog_info);
		notifBuilder.setContentTitle(getResources().getString(R.string.app_name));
		notifBuilder.setContentText(getResources().getString(R.string.dict_db_progressing));
		notifBuilder.setPriority(PRIORITY_DEFAULT);
		notifBuilder.setOnlyAlertOnce(true);
		return notifBuilder;
	}

	private boolean updateMain(int curPhraseId, NotificationCompat.Builder notifBuilder) {
		log("begin updating database from main data");
		NotificationManager notifManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
		int curWords = queryWordCount();
		notifBuilder.setProgress(WORDS_TOTAL, curWords, false);
		notifManager.notify(TKNotification.NOTIF_DICT_DB, notifBuilder.build());
		// try the first 100 inserts to measure speed
		int notifNext = curWords + 100;
		long start = System.currentTimeMillis();
		boolean result;
		try (InputStream ins = getAssets().open("01_詞目.csv")) {
			log("open phrases: ", ins);
			CsvListReader reader = new CsvListReader(new InputStreamReader(ins), CsvPreference.STANDARD_PREFERENCE);

			// first line is header
			List<String> data = reader.read();
			if (data == null)
				return true;

			int[] stats = new int[] {0, 0, 0};
			int cntWords = curWords;
			while (!dbStop.get()) {
				data = reader.read();
				if (data == null)
					break;

				// update progress
				if (cntWords >= notifNext) {
					notifBuilder.setProgress(WORDS_TOTAL, cntWords, false);
					notifManager.notify(TKNotification.NOTIF_DICT_DB, notifBuilder.build());
					// measure speed again
					long elapsed = System.currentTimeMillis() - start;
					if (elapsed <= 0) // too fast!
						elapsed = 1;
					// update once every 10 seconds in average
					int delta = (int)((cntWords - curWords) * 10000 / elapsed);
					if (delta <= 0) // too slow!
						delta = 1;
					notifNext += delta;
				}

				int phraseId = Integer.parseInt(data.get(0));
				if (phraseId < curPhraseId)
					continue;
				String phrase = data.get(3);
				if (phrase == null)
					continue;
				phrase = phrase.toLowerCase(Locale.ROOT);
				stats[0]++;

				String hans = data.get(2);
				// 20364,附錄,a-lú-mih,a-lú-mih,外來詞,20364(1)
				if (hans.equals(phrase))
					hans = "";

				// 26848,附錄,一人一家代，公媽隨人祀。,"Tsi̍t lâng tsi̍t ke tāi, kong-má suî-lâng tshāi.","全部,首字一畫",26848(1)
				// 26963,附錄,仙人拍鼓有時錯，跤步踏差啥人無？,"Sian-jîn phah kóo iú sî tshò, kha-pōo ta̍h-tsha siánn-lâng bô?","全部,首字五畫",26963(1)
				// 28029,附錄,臺北101(世貿),Tâi-pak It khòng it(Sè-bōo),臺北捷運淡水信義線,28029(1)
				String type = data.get(1);
				switch (type) {
					case "主詞目":
					case "單字不成詞者":
						updateWordWithAlts("phrase_id", phraseId, null, null, phrase, hans, stats);
						break;
					case "近反義詞不單列詞目者":
					case "附錄":
					case "臺華共同詞":
						updateWordsWithAlts("phrase_id", phraseId, null, null, phrase, hans, stats);
						break;
					default:
						log("unknown type: ", type);
						break;
				}

				cntWords = curWords + stats[1] + stats[2];
			}
			log("total phrases: " + stats[0]);
			log("total tailo words: ", stats[1]);
			log("total poj words: ", stats[2]);
			result = true;
		} catch (IOException ex) {
			log("read phrases error: ", ex);
			result = false;
		}
		log("end updating database from main data");
		return result;
	}

	private String queryHan(int phraseId) {
		Cursor c = db.rawQuery("select han from words where phrase_id=?",
		                       new String[] {String.valueOf(phraseId)});
		String han = null;
		if (c.moveToNext()) {
			han = c.getString(0);
		}
		c.close();
		return han;
	}

	private boolean updateAlts(int curAltId, String fileName, String altIdName, NotificationCompat.Builder notifBuilder) {
		log("begin updating database from alt data");
		NotificationManager notifManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
		int curWords = queryWordCount();
		notifBuilder.setProgress(WORDS_TOTAL, curWords, false);
		notifManager.notify(TKNotification.NOTIF_DICT_DB, notifBuilder.build());
		// try the first 100 inserts to measure speed
		int notifNext = curWords + 100;
		long start = System.currentTimeMillis();
		boolean result;
		int altId = 0;
		try (InputStream ins = getAssets().open(fileName)) {
			log("open alts: ", ins);
			CsvListReader reader = new CsvListReader(new InputStreamReader(ins), CsvPreference.STANDARD_PREFERENCE);

			// first line is header
			List<String> data = reader.read();
			if (data == null)
				return true;

			int[] stats = new int[] {0, 0, 0};
			int cntWords = curWords;
			while (!dbStop.get()) {
				data = reader.read();
				if (data == null)
					break;
				altId++;

				// update progress
				if (cntWords >= notifNext) {
					notifBuilder.setProgress(WORDS_TOTAL, cntWords, false);
					notifManager.notify(TKNotification.NOTIF_DICT_DB, notifBuilder.build());
					// measure speed again
					long elapsed = System.currentTimeMillis() - start;
					if (elapsed <= 0) // too fast!
						elapsed = 1;
					// update once every 10 seconds in average
					int delta = (int)((cntWords - curWords) * 10000 / elapsed);
					if (delta <= 0) // too slow!
						delta = 1;
					notifNext += delta;
				}

				if (altId < curAltId)
					continue;
				String word = data.get(2);
				if (word == null)
					continue;
				int phraseId = Integer.parseInt(data.get(0));
				String han = data.get(1);
				// 20574,草山,Iûnn-bîng-suann(陽明山)
				// 21045,草屯鎮,Tsháu-tūn（草屯）
				String[] wordWithHan = word.split("[()（）]");
				if (wordWithHan.length >= 2) {
					if (wordWithHan.length > 2) {
						log("unable to process alt: phraseId=", phraseId, " han=", han, " word=", word);
						continue;
					}
					word = wordWithHan[0];
					han = wordWithHan[1];
				}
				word = word.toLowerCase(Locale.ROOT);
				stats[0]++;

				// 20369,a̋i-sat-tsuh,a̋i-sat-tsirh
				// 20531,ua-khá-móo-tooh,oo-kám-má-tooh
				if (word.charAt(0) == han.charAt(0) || word.charAt(word.length()-1) == han.charAt(han.length()-1))
					han = "";

				updateWordWithAlts("phrase_id", phraseId, altIdName, altId, word, han, stats);
				cntWords = curWords + stats[1] + stats[2];
			}
			log("total words: " + stats[0]);
			log("total tailo words: ", stats[1]);
			log("total poj words: ", stats[2]);
			result = true;
		} catch (IOException ex) {
			log("read alts error: ", ex);
			result = false;
		}
		log("end updating database from alt data");
		return result;
	}

	private boolean updateWords(String fileName, String idName, int curId, NotificationCompat.Builder notifBuilder) {
		log("begin updating database from file: ", fileName);
		NotificationManager notifManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
		int curWords = queryWordCount();
		notifBuilder.setProgress(WORDS_TOTAL, curWords, false);
		notifManager.notify(TKNotification.NOTIF_DICT_DB, notifBuilder.build());
		// try the first 100 inserts to measure speed
		int notifNext = curWords + 100;
		long start = System.currentTimeMillis();
		boolean result;
		try (InputStream ins = getAssets().open(fileName)) {
			log("open phrases: ", ins);
			CsvListReader reader = new CsvListReader(new InputStreamReader(ins), CsvPreference.STANDARD_PREFERENCE);

			// first line is header
			List<String> data = reader.read();
			if (data == null)
				return true;

			int[] stats = new int[] {0, 0, 0};
			int cntWords = curWords;
			int id = 0;
			while (!dbStop.get()) {
				data = reader.read();
				if (data == null)
					break;
				id++;

				// update progress
				if (cntWords >= notifNext) {
					notifBuilder.setProgress(WORDS_TOTAL, cntWords, false);
					notifManager.notify(TKNotification.NOTIF_DICT_DB, notifBuilder.build());
					// measure speed again
					long elapsed = System.currentTimeMillis() - start;
					if (elapsed <= 0) // too fast!
						elapsed = 1;
					// update once every 10 seconds in average
					int delta = (int)((cntWords - curWords) * 10000 / elapsed);
					if (delta <= 0) // too slow!
						delta = 1;
					notifNext += delta;
				}

				if (id < curId)
					continue;
				String word = data.get(1);
				if (word == null)
					continue;
				word = word.toLowerCase(Locale.ROOT);
				stats[0]++;
				String han = data.get(0);
				updateWord(idName, id, true, null, null, word, han, stats);
				cntWords = curWords + stats[1] + stats[2];
			}
			log("total phrases: " + stats[0]);
			log("total tailo words: ", stats[1]);
			log("total poj words: ", stats[2]);
			result = true;
		} catch (IOException ex) {
			log("read phrases error: ", ex);
			result = false;
		}
		log("end updating database from file: ", fileName);
		return result;
	}

	private void updateWordsWithAlts(String idName, int idValue,
							 String altIdName, Integer altId,
							 String phrase, String han,
							 int[] stats) {
		String[] phraseAlts = new String[] {phrase};
		if (phrase.indexOf('/') >= 0) {
			// 17707,近反義詞不單列詞目者,𠢕囥歲,gâu khǹg-huè/gâu khǹg-hè,,
			phraseAlts = phrase.split("/");
		}

		for (String alt : phraseAlts)
			updateWords(idName, idValue, altIdName, altId, alt, han, stats);
	}

	private void updateWords(String idName, int idValue,
									String altIdName, Integer altId,
									String phrase, String han,
									int[] stats) {
		String[] words = phrase.split("[\\s(),─.?]+");
		StringBuilder hanb = new StringBuilder();
		// 26848,附錄,一人一家代，公媽隨人祀。,"Tsi̍t lâng tsi̍t ke tāi, kong-má suî-lâng tshāi.","全部,首字一畫",26848(1)
		// 26897,附錄,人飼人，一支骨；天飼人，肥朒朒。,"Lâng tshī lâng, tsi̍t ki kut; thinn tshī lâng, puî-tsut-tsut.","全部,首字二畫",26897(1)
		// 26963,附錄,仙人拍鼓有時錯，跤步踏差啥人無？,"Sian-jîn phah kóo iú sî tshò, kha-pōo ta̍h-tsha siánn-lâng bô?","全部,首字五畫",26963(1)
		// 28029,附錄,臺北101(世貿),Tâi-pak It khòng it(Sè-bōo),臺北捷運淡水信義線,28029(1)
		// 28090,附錄,富貴（南河）,Hù-kuì (Lâm-hô),支線——內灣線,28090(1)
		for (String str : han.split("[()（）。，─；？]+"))
			hanb.append(str);
		String hanChars = hanb.toString();
		int hanLen = hanChars.length();

		for (int i = 0; i < words.length; i++) {
			String word = words[i];
			String[] syls = word.split("-+");
			String hanWord = null;
			if (hanLen > 0) {
				if (syls.length > hanLen || i == words.length - 1) {
					hanWord = hanChars;
					hanChars = "";
					hanLen = 0;
				} else {
					hanWord = "";
					for (int j = 0; j < syls.length; j++) {
						char ch = hanChars.charAt(0);
						hanWord += ch;
						hanChars = hanChars.substring(1);
						if (Character.isHighSurrogate(ch)) {
							char ch2 = hanChars.charAt(0);
							hanWord += ch2;
							hanChars = hanChars.substring(1);
						}
					}
					if (hanWord.length() == 1) {
						// 28029,附錄,臺北101(世貿),Tâi-pak It khòng it(Sè-bōo),臺北捷運淡水信義線,28029(1)
						char ch = hanWord.charAt(0);
						if (ch >= '0' && ch <= '1')
							continue;
					}
				}
			} else {
				StringBuilder sylb = new StringBuilder(syls[0]);
				for (int j = 1; j < syls.length; j++) {
					sylb.append(',');
					sylb.append(syls[j]);
				}
				final String syl = sylb.toString();
				log("syls=", syl, "(", syls.length, ") han=", hanChars, "(", hanLen, ")");
			}
			updateWord(idName, idValue, false, altIdName, altId, word, hanWord, stats);
		}
	}

	private void updateWordWithAlts(String idName, int idValue,
									String altIdName, Integer altId,
									String word, String han,
									int[] stats) {
		String[] wordAlts = new String[] {word};
		if (word.indexOf('/') >= 0) {
			// 5,主詞目,一月日,tsi̍t gue̍h-ji̍t/tsi̍t ge̍h-li̍t,時間節令,5(1)
			wordAlts = word.split("/");
		}

		String[] hanAlts = new String[] {han};
		if (han.indexOf('(') >= 0) {
			// 21674,附錄,竿(菅)蓁林,Kuann-tsin-nâ,舊地名,
			int beginAlt = han.indexOf('(');
			int endAlt = han.indexOf(')');
			String han1 = han.substring(0, beginAlt) + han.substring(endAlt + 1);
			String han2 = han.substring(0, beginAlt - 1)
					+ han.substring(beginAlt + 1, endAlt)
					+ han.substring(endAlt + 1);
			hanAlts = new String[] {han1, han2};
		}

		for (String alt : wordAlts) {
			alt = alt.replaceAll("\\s", "-");
			for (String hanAlt : hanAlts) {
				if (hanAlt.isEmpty())
					hanAlt = null;
				updateWord(idName, idValue, true, altIdName, altId, alt, hanAlt, stats);
			}
		}
	}

	private void updateWord(String idName, int idValue, boolean idOverwrite,
							String altIdName, Integer altId,
							String word, String han,
							int[] stats) {
		String wordTl = wordTailoNormalized(word);
		if (wordTl.isEmpty()) {
			log("invalid word: ", word);
		} else {
			// 09_名: 塍,塍,1,晉、彙
			if (updateWord(idName, idValue, idOverwrite, altIdName, altId, wordTl, han, false))
				stats[1]++;
			String wordPoj = wordTailoToPoj(wordTl);
			if (updateWord(idName, idValue, idOverwrite, altIdName, altId, wordPoj, han, true))
				stats[2]++;
		}
	}

	private boolean updateWord(String idName, int idValue, boolean idOverwrite,
							   String altIdName, Integer altId,
							   String word, String han, boolean poj) {
		if (word == null || word.isEmpty() || word.matches("[\\s,;─.?/\"]")) {
			log("invalid word: ", word);
			return false;
		}
		if (han != null && (han.isEmpty() || han.matches("[\\s，─。？]"))) {
			log("invalid han for word ", word, ": ", han);
			return false;
		}
		String table = poj ? "words_poj" : "words";
		ContentValues values = new ContentValues();
		String toneless = new String(TKComposingUtils.removeTone(word));
		values.put("word", word);
		values.put("toneless", toneless);
		values.put(idName, idValue);
		if (altId != null)
			values.put(altIdName, altId);
		values.put("han", han);
		values.put("weight", 1.0d);
		long id = db.insertWithOnConflict(table, null, values, SQLiteDatabase.CONFLICT_IGNORE);
		if (id == -1L) {
			/*
			try (Cursor c = db.rawQuery("select * from " + table + " where word=? and han=?", new String[] {word, han})) {
				if (c.moveToNext()) {
					log(poj ? "POJ" : "TL", " word ", word, " (hanji=", han, ") conflicts with data:");
					int cols = c.getColumnCount();
					for (int i = 0; i < cols; i++) {
						log("column#", i, ": ", c.getColumnName(i), "=", c.getString(i));
					}
				}
			}
			*/
			if (idOverwrite || altId != null) {
				values = new ContentValues();
				if (idOverwrite)
					values.put(idName, idValue);
				// migration from words table without alt_id
				if (altId != null)
					values.put(altIdName, altId);
				db.updateWithOnConflict(table, values, "word=? and han=?", new String[]{word, han}, SQLiteDatabase.CONFLICT_IGNORE);
			}
		}
		return id != -1L;
	}

	private void incrementWordWeight(String word) {
		word = word.toLowerCase(Locale.ROOT);
		String[] args = new String[] {word};
		// for TL
		try (Cursor c = db.rawQuery("select weight from words where word=?", args)) {
			while (c.moveToNext()) {
				double weight = c.getDouble(0);
				ContentValues values = new ContentValues();
				double newWeight = Math.sqrt(weight + 1.0d);
				values.put("weight", newWeight);
				db.update("words", values, "word=?", args);
				log("update weight of TL word ", word, " from ", weight, " to ", newWeight);
			}
		}
		// for TL Han
		try (Cursor c = db.rawQuery("select weight from words where han=?", args)) {
			while (c.moveToNext()) {
				double weight = c.getDouble(0);
				ContentValues values = new ContentValues();
				double newWeight = Math.sqrt(weight + 1.0d);
				values.put("weight", newWeight);
				db.update("words", values, "han=?", args);
				log("update weight of TL han ", word, " from ", weight, " to ", newWeight);
			}
		}
		// for POJ
		try (Cursor c = db.rawQuery("select weight from words_poj where word=?", args)) {
			while (c.moveToNext()) {
				double weight = c.getDouble(0);
				ContentValues values = new ContentValues();
				double newWeight = Math.sqrt(weight + 1.0d);
				values.put("weight", newWeight);
				db.update("words_poj", values, "word=?", args);
				log("update weight of POJ word ", word, " from ", weight, " to ", newWeight);
			}
		}
		// for POJ Han
		try (Cursor c = db.rawQuery("select weight from words_poj where han=?", args)) {
			while (c.moveToNext()) {
				double weight = c.getDouble(0);
				ContentValues values = new ContentValues();
				double newWeight = Math.sqrt(weight + 1.0d);
				values.put("weight", newWeight);
				db.update("words_poj", values, "han=?", args);
				log("update weight of POJ han ", word, " from ", weight, " to ", newWeight);
			}
		}
	}

	private void migrateTable(String name) {
		String nameOrg = name + "_org";
		if (!TKDatabase.checkTableExist(db, nameOrg))
			return;
		// migrate weights
		String index = "idx_" + nameOrg + "_with_weight";
		db.execSQL("drop index if exists " + index);
		db.execSQL("create index " + index + " on " + nameOrg + " (weight)");
		try (Cursor c = db.rawQuery("select word, weight from " + nameOrg + " where weight > 1", new String[] {})) {
			while (c.moveToNext()) {
				String word = c.getString(0);
				double weight = c.getDouble(1);
				ContentValues values = new ContentValues();
				values.put("weight", weight);
				db.update(name, values, "word=?", new String[] {word});
			}
		}
		db.execSQL("drop index if exists " + index);
		db.execSQL("drop table " + nameOrg);
	}
}
