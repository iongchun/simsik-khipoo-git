心適ê鍵盤(Sim-sik ê Khí-pooh)隱私權政策

此程式:
- 指的是此連結的應用程式: https://play.google.com/store/apps/details?id=tw.iongchun.taigikbd
- 開發人員為: Ang Iongchun
- 僅會收集詞彙點擊頻率, 以供選字功能顯示使用 
- 不會傳送任何個人資訊至任何手機外的位置
- 可以透過 Android 系統功能刪除任何應用程式收集的資訊 
