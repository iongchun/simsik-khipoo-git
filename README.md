# README #

### Siāu-kài ###

* Tse sī Android app ["Sim-sik ê Khí-pooh"](https://play.google.com/store/apps/details?id=tw.iongchun.taigikbd) ê guân-thâu-bé
* "Sim-sik ê Khí-pooh" sī siat-kè iōng-lâi siá Tâi-gí Lô-má-jī
* Bo̍k-tsiân ê pán-pún sî 1.7.3
* Tshiánn iōng Android Studio 2024.3.1 í-siōng lâi sú-iōng

### Siā-kûn ###

* Thó-lūn ê sóo-tsāi: [Google Groups](https://groups.google.com/d/forum/simsik-discuss)
* Guân-thâu-bé: [BitBucket](https://bitbucket.org/iongchun/simsik-khipoo-git)
