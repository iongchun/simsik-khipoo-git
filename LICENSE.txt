Sim-sik ê Khí-pooh sī iû Âng Iōngchun tsè-tsok,
àn-tsiàu GNU it-puann kong-kiōng pàng-khuân tiâu-khuán (GPL) 3.0 huat-hîng.

Līng-guā, Sim-sik ê Khí-pooh iáu-koh ū pau-hâm ē-kha ê nńg-thé kah tsu-liāu.
In ê pán-khuân lóng sī in ê tsè-tsok-lâng sóo-iú, páng-khuân ê khuán-sit sī:

Sim-Sik ê jī-uē: CC BY-SA 4.0
https://creativecommons.org/licenses/by-sa/4.0/

PCollections: MIT License
https://raw.githubusercontent.com/hrldcpr/pcollections/master/LICENSE

Super CSV: Apache License 2.0
http://super-csv.github.io/super-csv/license.html

Kàu-io̍k-pōo ê Tâi-uân Bām-lâm-gú Thong-thò-uē Sû-tián: CC BY-ND 3.0 TW
https://creativecommons.org/licenses/by-nd/3.0/tw/
